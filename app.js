function distance(first, second){
	if(first.length - second.length > 0)
		return 1;
		
    if(first.length==0 || second.length==0 )
		return 0;

	if(first !== second)
		return 2;
	
	if(!Array.isArray(first) || !Array.isArray(second))
		throw new Error('InvalidType');
 
}



module.exports.distance = distance